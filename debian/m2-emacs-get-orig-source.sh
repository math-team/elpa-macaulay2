#!/bin/bash

# get M2-emacs orig tarball

set -e

TEMP=$(getopt -o "r:p:" -l "ref,path" -n "m2-emacs-get-orig-source" \
	      -- $@)

if [ $? -ne 0 ]; then
        echo 'Terminating...' >&2
        exit 1
fi

eval set -- "$TEMP"
unset TEMP

while true; do
    case "$1" in
	'-r'|'--ref')
	    REF=$2
	    shift 2
	    continue
	    ;;
	'-p'|'--path')
	    M2_PATH=$2
	    shift 2
	    break
	    ;;
	'--')
	    shift
	    break
	    ;;
	*)
	    echo 'Internal error!' >&2
	    exit 1
	    ;;
    esac
done

REMOTE="Macaulay2"

if [ -z $REF ]
then
    echo "no version specified; using master branch ..."
    REF="master"
fi

M2_EMACS_PATH="M2/Macaulay2/editors/emacs"

if [ -z $M2_PATH ]
then
    echo -n "cloning temporary M2 repository ... "
    pushd $(mktemp -d) > /dev/null

    git clone -q --filter=blob:none --no-checkout \
	https://github.com/${REMOTE}/M2 -b $REF

    cd M2
    echo "done"
else
    echo "using M2 repository at $M2_PATH"
    pushd $M2_PATH > /dev/null
fi

echo -n "determining M2-emacs commit ... "
git checkout $REF -- $M2_EMACS_PATH
M2_EMACS_REF=$(git ls-tree $REF -- $M2_EMACS_PATH | awk '{print $3}')
echo $M2_EMACS_REF

echo -n "determining corresponding M2 commit ... "
GIT_COMMIT=$(git rev-list -1 $REF $M2_EMACS_PATH)
echo $GIT_COMMIT

echo -n "determining version number ... "
git checkout $REF -- M2/VERSION 2> /dev/null
GIT_VERSION=$(git show $REF:M2/VERSION)
NEW_COMMITS=$(git rev-list \
		  $(git rev-list -1 $REF M2/VERSION)..$GIT_COMMIT \
		  --count)
VERSION=$GIT_VERSION+git$NEW_COMMITS.$(echo $GIT_COMMIT | cut -c 1-7)
echo $VERSION

popd > /dev/null

TARBALL=../elpa-macaulay2_$VERSION.orig.tar

git fetch -q https://github.com/${REMOTE}/M2-emacs $M2_EMACS_REF
git archive -o $TARBALL $M2_EMACS_REF
xz -fz $TARBALL
echo "orig tarball: $TARBALL.xz"
